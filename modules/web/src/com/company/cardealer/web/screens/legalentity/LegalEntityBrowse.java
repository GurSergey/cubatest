package com.company.cardealer.web.screens.legalentity;

import com.company.cardealer.entity.Individual;
import com.company.cardealer.service.CountRequestsService;
import com.haulmont.cuba.gui.UiComponents;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.LegalEntity;

import javax.inject.Inject;
import java.util.List;

@UiController("cardealer_LegalEntity.browse")
@UiDescriptor("legal-entity-browse.xml")
@LookupComponent("legalEntitiesTable")
@LoadDataBeforeShow
public class LegalEntityBrowse extends StandardLookup<LegalEntity> {
    @Inject
    CountRequestsService service;

    @Inject
    private GroupTable<LegalEntity> legalEntitiesTable;

    @Inject
    CollectionContainer<LegalEntity> legalEntitiesDc;

    @Inject
    UiComponents uiComponents;


    @Subscribe
    protected void onBeforeShow(BeforeShowEvent event) {

        legalEntitiesTable.addGeneratedColumn("numbAuto", new Table.ColumnGenerator<LegalEntity>() {
            @Override
            public Component generateCell(LegalEntity entity) {
                Label field = uiComponents.create(Label.NAME);
                field.setValue(service.getCountForCustomer(entity));
                return field;
            }
        });
        legalEntitiesTable.addPrintable("numbAuto", new Table.Printable<LegalEntity, String>() {
            @Override
            public String getValue(LegalEntity entity) {
                return String.valueOf(service.getCountForCustomer(entity));
            }
        });
    }
}