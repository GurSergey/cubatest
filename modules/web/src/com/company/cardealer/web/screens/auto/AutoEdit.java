package com.company.cardealer.web.screens.auto;

import com.company.cardealer.entity.CarMaker;
import com.company.cardealer.entity.Equipment;
import com.company.cardealer.entity.Model;
import com.company.cardealer.web.screens.model.ModelBrowse;
import com.company.cardealer.web.screens.model.ModelBrowseByCarMaker;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.TimeSource;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.Auto;

import javax.inject.Inject;
import java.util.Date;

@UiController("cardealer_Auto.edit")
@UiDescriptor("auto-edit.xml")
@EditedEntityContainer("autoDc")
@LoadDataBeforeShow
public class AutoEdit extends StandardEditor<Auto> {

    @Inject
    private ScreenBuilders screenBuilders;

    @Inject
    private DateField<Date> yearField;

    @Inject
    private PickerField<Model> modelField;

    @Inject
    private PickerField<Equipment> equipmentField;

    @Inject
    private TextArea<String> nameField;

    @Inject
    private PickerField<CarMaker> makerField;

    @Inject
    private TextField<Integer> costField;

    @Inject
    private Screens screens;

    public AutoEdit() {
    }

    @Subscribe("modelField.lookup")
    protected void onModelFieldLookupActionPerformed(Action.ActionPerformedEvent event)
    {

        ModelBrowseByCarMaker browse = screenBuilders.lookup(Model.class, this).
                withScreenClass(ModelBrowseByCarMaker.class).
                withField(modelField).
                withLaunchMode(OpenMode.DIALOG).
                build();
        browse.setMaker(makerField.getValue());
        browse.show();
        equipmentField.setEnabled(true);

    }

    @Subscribe("modelField.clear")
    protected void onModelFieldClearActionPerformed(Action.ActionPerformedEvent event) {
        modelField.clear();
        equipmentField.setEnabled(false);
    }

    @Subscribe
    protected void onInit(InitEvent event) {
        yearField.setValue(AppBeans.get(TimeSource.class).currentTimestamp());
        nameField.setEnabled(false);
        equipmentField.setEnabled(false);
    }

}