alter table CARDEALER_COLOR add column DELETED_BY varchar(50) ;
alter table CARDEALER_COLOR add column UPDATE_TS timestamp ;
alter table CARDEALER_COLOR add column DELETE_TS timestamp ;
alter table CARDEALER_COLOR add column UPDATED_BY varchar(50) ;
alter table CARDEALER_COLOR add column CREATED_BY varchar(50) ;
alter table CARDEALER_COLOR add column CREATE_TS timestamp ;
alter table CARDEALER_COLOR add column VERSION integer ^
update CARDEALER_COLOR set VERSION = 0 where VERSION is null ;
alter table CARDEALER_COLOR alter column VERSION set not null ;
