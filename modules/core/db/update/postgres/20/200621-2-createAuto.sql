alter table CARDEALER_AUTO add constraint FK_CARDEALER_AUTO_ON_MAKER foreign key (MAKER_ID) references CARDEALER_CAR_MAKER(ID);
alter table CARDEALER_AUTO add constraint FK_CARDEALER_AUTO_ON_MODEL foreign key (MODEL_ID) references CARDEALER_MODEL(ID);
alter table CARDEALER_AUTO add constraint FK_CARDEALER_AUTO_ON_EQUIPMENT foreign key (EQUIPMENT_ID) references CARDEALER_EQUIPMENT(ID);
create index IDX_CARDEALER_AUTO_ON_MAKER on CARDEALER_AUTO (MAKER_ID);
create index IDX_CARDEALER_AUTO_ON_MODEL on CARDEALER_AUTO (MODEL_ID);
create index IDX_CARDEALER_AUTO_ON_EQUIPMENT on CARDEALER_AUTO (EQUIPMENT_ID);
