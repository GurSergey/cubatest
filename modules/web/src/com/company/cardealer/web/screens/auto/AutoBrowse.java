package com.company.cardealer.web.screens.auto;

import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.Auto;

@UiController("cardealer_Auto.browse")
@UiDescriptor("auto-browse.xml")
@LookupComponent("autosTable")
@LoadDataBeforeShow
public class AutoBrowse extends StandardLookup<Auto> {
}