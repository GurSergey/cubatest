package com.company.cardealer.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Extends;

import javax.persistence.*;

@NamePattern("%s|name")
@PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
@Table(name = "CARDEALER_LEGAL_ENTITY")
@Entity(name = "cardealer_LegalEntity")
//@Extends(Customer.class)
public class LegalEntity extends Customer {
    private static final long serialVersionUID = -8429967508363717748L;



    @Lob
    @Column(name = "INN_ADDRESS")
    protected String innAddress;

    @Column(name = "PHONE", length = 11)
    protected String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInnAddress() {
        return innAddress;
    }

    public void setInnAddress(String innAddress) {
        this.innAddress = innAddress;
    }


}