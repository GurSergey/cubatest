package com.company.cardealer.service;

import com.company.cardealer.entity.Auto;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.listener.BeforeInsertEntityListener;
import com.haulmont.cuba.core.listener.BeforeUpdateEntityListener;
import org.springframework.stereotype.Component;

@Component("AutoListener")
public class AutoListener implements
        BeforeInsertEntityListener<Auto>,
        BeforeUpdateEntityListener<Auto> {

    @Override
    public void onBeforeInsert(Auto entity, EntityManager entityManager) {
        entity.setName( entity.getMaker().getName()+" "+ entity.getModel().getName()
                +" " + entity.getEquipment().getName());
    }

    @Override
    public void onBeforeUpdate(Auto entity, EntityManager entityManager) {
        entity.setName( entity.getMaker().getName()+" "+ entity.getModel().getName()
                +" " + entity.getEquipment().getName());
    }
}
