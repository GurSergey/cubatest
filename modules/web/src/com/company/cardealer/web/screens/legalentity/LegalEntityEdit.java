package com.company.cardealer.web.screens.legalentity;

import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.LegalEntity;

@UiController("cardealer_LegalEntity.edit")
@UiDescriptor("legal-entity-edit.xml")
@EditedEntityContainer("legalEntityDc")
@LoadDataBeforeShow
public class LegalEntityEdit extends StandardEditor<LegalEntity> {
}