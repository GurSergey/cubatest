package com.company.cardealer.web.screens.carmaker;

import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.CarMaker;

@UiController("cardealer_CarMaker.edit")
@UiDescriptor("car-maker-edit.xml")
@EditedEntityContainer("carMakerDc")
@LoadDataBeforeShow
public class CarMakerEdit extends StandardEditor<CarMaker> {
}