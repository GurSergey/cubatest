create table CARDEALER_AUTO (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    MAKER_ID uuid,
    MODEL_ID uuid,
    EQUIPMENT_ID uuid,
    NAME text,
    YEAR_ date,
    COST integer,
    --
    primary key (ID)
);