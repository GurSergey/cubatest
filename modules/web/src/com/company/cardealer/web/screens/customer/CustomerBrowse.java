package com.company.cardealer.web.screens.customer;

import com.company.cardealer.entity.Individual;
import com.company.cardealer.service.CountRequestsService;
import com.haulmont.cuba.gui.UiComponents;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.Customer;

import javax.inject.Inject;

@UiController("cardealer_Customer.browse")
@UiDescriptor("customer-browse.xml")
@LookupComponent("customersTable")
@LoadDataBeforeShow
public class CustomerBrowse extends StandardLookup<Customer> {
    @Inject
    CountRequestsService service;

    @Inject
    private GroupTable<Customer> customersTable;

    @Inject
    CollectionContainer<Customer> customersDc;

    @Inject
    UiComponents uiComponents;
    @Subscribe
    protected void onBeforeShow(BeforeShowEvent event) {
        customersTable.addGeneratedColumn("numbAuto", new Table.ColumnGenerator<Customer>() {
            @Override
            public Component generateCell(Customer entity) {
                Label field = uiComponents.create(Label.NAME);
                field.setValue(service.getCountForCustomer(entity));
                return field;
            }
        });
        customersTable.addPrintable("numbAuto", new Table.Printable<Customer, String>() {
            @Override
            public String getValue(Customer entity) {
                return String.valueOf(service.getCountForCustomer(entity));
            }
        });
    }
}