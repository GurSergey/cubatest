package com.company.cardealer.web.screens.individual;

import com.company.cardealer.entity.LegalEntity;
import com.company.cardealer.service.CountRequestsService;
import com.haulmont.cuba.gui.UiComponents;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.DataLoader;
import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.Individual;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import org.apache.poi.ss.formula.functions.Column;
import org.eclipse.persistence.jpa.jpql.tools.resolver.Declaration;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

@UiController("cardealer_Individual.browse")
@UiDescriptor("individual-browse.xml")
@LookupComponent("individualsTable")
@LoadDataBeforeShow
public class IndividualBrowse extends StandardLookup<Individual> {
    @Inject
    CountRequestsService service;

    @Inject
    private GroupTable<Individual> individualsTable;

    @Inject
    CollectionContainer<Individual> individualsDc;

    @Inject
    UiComponents uiComponents;

    @Subscribe
    protected void onBeforeShow(BeforeShowEvent event) {
        individualsTable.addGeneratedColumn("numbAuto", new Table.ColumnGenerator<Individual>() {
            @Override
            public Component generateCell(Individual entity) {
                Label field = uiComponents.create(Label.NAME);
                field.setValue(service.getCountForCustomer(entity));
                return field;
            }
        });
        individualsTable.addPrintable("numbAuto", new Table.Printable<Individual, String>() {
            @Override
            public String getValue(Individual entity) {
                return String.valueOf(service.getCountForCustomer(entity));
            }
        });
    }


}