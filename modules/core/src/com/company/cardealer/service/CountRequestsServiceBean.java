package com.company.cardealer.service;

import com.company.cardealer.entity.Customer;
import com.company.cardealer.entity.Request;
import com.haulmont.cuba.core.*;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service(CountRequestsService.NAME)
public class CountRequestsServiceBean implements CountRequestsService {

    @Inject
    private Persistence persistence;

    @Override
    public Long getCountForCustomer(Customer customer) {

        try(Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();
            Query query =
                    em.createQuery("SELECT COUNT(r) FROM cardealer_Reques r WHERE r.customer = :customer ",
                            Long.class);
            Long res = (Long) query.setParameter("customer", customer).getFirstResult();
            tx.commit();
            return res;
        }
    }
}