package com.company.cardealer.web.screens.request;

import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.Request;

@UiController("cardealer_Reques.browse")
@UiDescriptor("request-browse.xml")
@LookupComponent("requestsTable")
@LoadDataBeforeShow
public class RequestBrowse extends StandardLookup<Request> {
}