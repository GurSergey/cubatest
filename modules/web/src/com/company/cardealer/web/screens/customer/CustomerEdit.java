package com.company.cardealer.web.screens.customer;

import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.Customer;

@UiController("cardealer_Customer.edit")
@UiDescriptor("customer-edit.xml")
@EditedEntityContainer("customerDc")
@LoadDataBeforeShow
public class CustomerEdit extends StandardEditor<Customer> {
}