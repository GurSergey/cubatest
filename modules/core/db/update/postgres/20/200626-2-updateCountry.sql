alter table CARDEALER_COUNTRY add column DELETED_BY varchar(50) ;
alter table CARDEALER_COUNTRY add column UPDATE_TS timestamp ;
alter table CARDEALER_COUNTRY add column DELETE_TS timestamp ;
alter table CARDEALER_COUNTRY add column UPDATED_BY varchar(50) ;
alter table CARDEALER_COUNTRY add column CREATED_BY varchar(50) ;
alter table CARDEALER_COUNTRY add column CREATE_TS timestamp ;
alter table CARDEALER_COUNTRY add column VERSION integer ^
update CARDEALER_COUNTRY set VERSION = 0 where VERSION is null ;
alter table CARDEALER_COUNTRY alter column VERSION set not null ;
