package com.company.cardealer.entity;

import com.company.cardealer.service.CountryService;
import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseUuidEntity;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.global.AppBeans;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.*;
import java.util.List;

@NamePattern("%s|name")
@Table(name = "CARDEALER_CAR_MAKER")
@Entity(name = "cardealer_CarMaker")
public class CarMaker extends StandardEntity {
    private static final long serialVersionUID = 944187749852930571L;
    @Column(name = "NAME", unique = true)
    protected String name;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COUNTRY_ID")
    protected Country country;
    @Column(name = "CODE", unique = true)
    protected String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Model> getModels() {
        return models;
    }

    public void setModels(List<Model> models) {
        this.models = models;
    }

    @OneToMany(mappedBy = "maker")
    private List<Model> models;

    @PostConstruct
    private void initCountry() {
        if (country == null) {
            CountryService countryService = AppBeans.get(CountryService.class);
            country = countryService.getDefaultCountry();
        }
    }
}