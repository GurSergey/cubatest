package com.company.cardealer.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum TypeAuto implements EnumClass<Integer> {

    CROSSOVER(10),
    WAGON(20),
    SEDAN(30);

    private Integer id;

    TypeAuto(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static TypeAuto fromId(Integer id) {
        for (TypeAuto at : TypeAuto.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}