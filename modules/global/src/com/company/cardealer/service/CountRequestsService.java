package com.company.cardealer.service;

import com.company.cardealer.entity.Customer;

public interface CountRequestsService {
    String NAME = "cardealer_CountRequestsService";
    public Long getCountForCustomer(Customer customer);
}