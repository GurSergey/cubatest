package com.company.cardealer.web.screens.model;

import com.company.cardealer.entity.CarMaker;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.Model;

import javax.inject.Inject;

@UiController("cardealer_Model.browseByCarMaker")
@UiDescriptor("model-browse-by-carmaker.xml")
@LookupComponent("modelsTable")
public class ModelBrowseByCarMaker extends StandardLookup<Model> {

    @Inject
    private CollectionLoader<Model> modelsDl;

    public void setMaker(CarMaker maker) {
        this.maker = maker;
    }

    private CarMaker maker;

    @Subscribe
    protected void onBeforeShow(BeforeShowEvent event) {
        modelsDl.setParameter("maker", maker);
        modelsDl.load();
    }

}