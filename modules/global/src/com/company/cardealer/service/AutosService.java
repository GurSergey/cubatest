package com.company.cardealer.service;

import com.company.cardealer.entity.Auto;

import java.util.List;

public interface AutosService {
    String NAME = "cardealer_AutosService";
    List<Auto> getListAutos();
}