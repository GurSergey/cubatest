package com.company.cardealer.web.screens;

import java.util.function.Function;

public class PhoneFormatter implements Function<String, String> {


    @Override
    public String apply(String phone) {
        StringBuilder builder = new StringBuilder(phone);
        return builder.insert(1,' ').
                insert(5, ' ').
                insert(9, ' ').
                insert(12, ' ').
                toString();
    }
}
