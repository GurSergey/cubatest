alter table CARDEALER_MODEL add column DELETED_BY varchar(50) ;
alter table CARDEALER_MODEL add column UPDATE_TS timestamp ;
alter table CARDEALER_MODEL add column DELETE_TS timestamp ;
alter table CARDEALER_MODEL add column UPDATED_BY varchar(50) ;
alter table CARDEALER_MODEL add column CREATED_BY varchar(50) ;
alter table CARDEALER_MODEL add column CREATE_TS timestamp ;
alter table CARDEALER_MODEL add column VERSION integer ^
update CARDEALER_MODEL set VERSION = 0 where VERSION is null ;
alter table CARDEALER_MODEL alter column VERSION set not null ;
