create table CARDEALER_REQUES (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    AUTO_ID uuid,
    CUSTOMER_ID uuid,
    PAID boolean,
    COST integer,
    MANAGER_ID uuid,
    --
    primary key (ID)
);