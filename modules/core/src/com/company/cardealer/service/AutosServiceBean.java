package com.company.cardealer.service;

import com.company.cardealer.entity.Auto;
import com.company.cardealer.entity.Request;
import com.haulmont.cuba.core.*;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service(AutosService.NAME)
public class AutosServiceBean implements AutosService {
    @Inject
    private Persistence persistence;
    @Override
    public List<Auto> getListAutos() {
        try(Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();
            TypedQuery<Auto> query =
                    em.createQuery("SELECT c FROM cardealer_Auto c WHERE c.id NOT IN (SELECT r.auto FROM cardealer_Reques r) ",
                            Auto.class);
            List<Auto> autos = query.getResultList();
            tx.commit();
            return autos;
        }
    }
}