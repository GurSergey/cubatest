package com.company.cardealer.core;

import com.haulmont.cuba.core.config.Config;
import com.haulmont.cuba.core.config.Property;
import com.haulmont.cuba.core.config.Source;
import com.haulmont.cuba.core.config.SourceType;

@Source(type = SourceType.APP)
public interface CountryConfig extends Config {
    @Property("cardealer.defaultCountry.name")
    String getCountryName();
    @Property("cardealer.defaultCountry.code")
    String getCountryCode();
}