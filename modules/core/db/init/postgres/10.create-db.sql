-- begin CARDEALER_MODEL
create table CARDEALER_MODEL (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    MAKER_ID uuid,
    --
    primary key (ID)
)^
-- end CARDEALER_MODEL
-- begin CARDEALER_COLOR
create table CARDEALER_COLOR (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    CODE varchar(255),
    --
    primary key (ID)
)^
-- end CARDEALER_COLOR
-- begin CARDEALER_CAR_MAKER
create table CARDEALER_CAR_MAKER (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    COUNTRY_ID uuid,
    CODE varchar(255),
    --
    primary key (ID)
)^
-- end CARDEALER_CAR_MAKER
-- begin CARDEALER_COUNTRY
create table CARDEALER_COUNTRY (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    CODE varchar(10),
    --
    primary key (ID)
)^
-- end CARDEALER_COUNTRY
-- begin CARDEALER_CUSTOMER
create table CARDEALER_CUSTOMER (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    DTYPE varchar(100),
    --
    NAME varchar(255),
    PHONE varchar(11),
    --
    primary key (ID)
)^
-- end CARDEALER_CUSTOMER
-- begin CARDEALER_AUTO
create table CARDEALER_AUTO (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    MAKER_ID uuid,
    MODEL_ID uuid,
    EQUIPMENT_ID uuid,
    NAME text,
    YEAR_ date,
    COST integer,
    --
    primary key (ID)
)^
-- end CARDEALER_AUTO
-- begin CARDEALER_REQUES
create table CARDEALER_REQUES (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    AUTO_ID uuid,
    CUSTOMER_ID uuid,
    PAID boolean,
    COST integer,
    MANAGER_ID uuid,
    --
    primary key (ID)
)^
-- end CARDEALER_REQUES
-- begin CARDEALER_EQUIPMENT
create table CARDEALER_EQUIPMENT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    COST integer,
    TYPE_ integer,
    DESCRIPTION text,
    --
    primary key (ID)
)^
-- end CARDEALER_EQUIPMENT
-- begin CARDEALER_INDIVIDUAL
create table CARDEALER_INDIVIDUAL (
    ID uuid,
    --
    primary key (ID)
)^
-- end CARDEALER_INDIVIDUAL
-- begin CARDEALER_LEGAL_ENTITY
create table CARDEALER_LEGAL_ENTITY (
    ID uuid,
    --
    INN_ADDRESS text,
    PHONE varchar(11),
    --
    primary key (ID)
)^
-- end CARDEALER_LEGAL_ENTITY

-- begin SEC_USER
alter table SEC_USER add column COUNTRY_ID uuid ^
alter table SEC_USER add column DTYPE varchar(100) ^
update SEC_USER set DTYPE = 'cardealer_ExtUser' where DTYPE is null ^
-- end SEC_USER
