package com.company.cardealer.entity;

import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Extends;
import com.haulmont.cuba.security.entity.User;

import javax.jws.soap.SOAPBinding;
import javax.persistence.*;

@Entity(name = "cardealer_ExtUser")
@Extends(User.class)
public class ExtUser extends User {
    private static final long serialVersionUID = 7642791899654588703L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COUNTRY_ID")
    protected Country country;

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}