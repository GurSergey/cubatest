package com.company.cardealer.web.screens.request;

import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.Request;

@UiController("cardealer_Reques.edit")
@UiDescriptor("request-edit.xml")
@EditedEntityContainer("requestDc")
@LoadDataBeforeShow
public class RequestEdit extends StandardEditor<Request> {
}