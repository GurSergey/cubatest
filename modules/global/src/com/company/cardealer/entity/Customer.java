package com.company.cardealer.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;

@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "CARDEALER_CUSTOMER")
@Entity(name = "cardealer_Customer")
public class Customer extends StandardEntity {
    private static final long serialVersionUID = 4183156551239716175L;
    @Column(name = "NAME")
    protected String name;
    @Column(name = "PHONE", length = 11)
    protected String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}