package com.company.cardealer.web.screens.model;

import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.Model;

@UiController("cardealer_Model.edit")
@UiDescriptor("model-edit.xml")
@EditedEntityContainer("modelDc")
@LoadDataBeforeShow
public class ModelEdit extends StandardEditor<Model> {
}