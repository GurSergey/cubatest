alter table CARDEALER_CAR_MAKER add column DELETED_BY varchar(50) ;
alter table CARDEALER_CAR_MAKER add column UPDATE_TS timestamp ;
alter table CARDEALER_CAR_MAKER add column DELETE_TS timestamp ;
alter table CARDEALER_CAR_MAKER add column UPDATED_BY varchar(50) ;
alter table CARDEALER_CAR_MAKER add column CREATED_BY varchar(50) ;
alter table CARDEALER_CAR_MAKER add column CREATE_TS timestamp ;
alter table CARDEALER_CAR_MAKER add column VERSION integer ^
update CARDEALER_CAR_MAKER set VERSION = 0 where VERSION is null ;
alter table CARDEALER_CAR_MAKER alter column VERSION set not null ;
