package com.company.cardealer.service;

import com.company.cardealer.core.CountryConfig;
import com.company.cardealer.entity.Country;
import com.haulmont.cuba.core.global.Metadata;
import org.jgroups.blocks.GridFile;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service(CountryService.NAME)
public class CountryServiceBean implements CountryService {
    @Inject
    CountryConfig config;
    @Inject
    Metadata metadata;

    @Override
    public Country getDefaultCountry() {
        Country country = metadata.create(Country.class);
        country.setCode(config.getCountryCode());
        country.setName(config.getCountryName());
        return country;
    }
}