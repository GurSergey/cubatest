package com.company.cardealer.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

@NamePattern("%s|name")
@Table(name = "CARDEALER_EQUIPMENT")
@Entity(name = "cardealer_Equipment")
public class Equipment extends StandardEntity {
    private static final long serialVersionUID = -8913822692966885540L;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "COST")
    protected Integer cost;

    @Column(name = "TYPE_")
    protected Integer type;

    @Lob
    @Column(name = "DESCRIPTION")
    protected String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TypeAuto getType() {
        return type == null ? null : TypeAuto.fromId(type);
    }

    public void setType(TypeAuto type) {
        this.type = type == null ? null : type.getId();
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}