package com.company.cardealer.web.screens.model;

import com.haulmont.cuba.gui.screen.*;
import com.company.cardealer.entity.Model;

@UiController("cardealer_Model.browse")
@UiDescriptor("model-browse.xml")
@LookupComponent("modelsTable")
@LoadDataBeforeShow
public class ModelBrowse extends StandardLookup<Model> {
}